(function(document) {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'login': '/',
      'page': '/page'
    }
  });

}(document));
